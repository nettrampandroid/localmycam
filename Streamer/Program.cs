﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using AForge.Video;
using AForge.Video.DirectShow;


namespace Streamer
{
    class Program
    {

        const int SW_HIDE = 0;
        


        [DllImport("kernel32.dll")]

        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]

        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);



        private static IPEndPoint clientEndPoint;
        private static UdpClient udpClient = new UdpClient();
        static void Main(string[] args)
        {
            var watcherIp = ConfigurationManager.AppSettings.Get("ip");
            var port = int.Parse(ConfigurationManager.AppSettings.Get("port"));
            clientEndPoint = new IPEndPoint(IPAddress.Parse(watcherIp), port);
            
            Console.WriteLine($"watcher: {clientEndPoint}");

            FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            VideoCaptureDevice videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString);
            videoSource.NewFrame += VideoSource_NewFrame;
            videoSource.Start();

            Console.WriteLine("Нажмите Enter чтобы скрыть програму");
            Console.ReadLine();
            ShowWindow(GetConsoleWindow(), SW_HIDE);


        }

        private static void VideoSource_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
        {
          var bmp = new Bitmap(eventArgs.Frame, 800, 600);
            try
            {
                using (var ms = new MemoryStream())
                {
                    bmp.Save(ms, ImageFormat.Jpeg);
                    var bytes = ms.ToArray();
                    udpClient.Send(bytes, bytes.Length, clientEndPoint);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }
    }
}   
